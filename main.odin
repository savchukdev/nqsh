package main

import "core:fmt"
import "core:intrinsics"
import "core:sys/unix"
import "core:os"
import "core:io"
import "core:strings"
import "core:c/libc"
import "core:time"
import "core:runtime"

sys_execve :: proc "contextless" (program: cstring, args: []cstring, envp: []cstring) -> int {
	return int(intrinsics.syscall(
    unix.SYS_execve,
    uintptr(rawptr(program)),
    uintptr(raw_data(args)),
    uintptr(raw_data(envp)),
  ))
}

// wait for a child process to complete
wait_for_child :: proc "contextless" () -> int {
  status := 0
  intrinsics.syscall(unix.SYS_wait4, 0, uintptr(&status), 0, 0)

  return status
}

// filename should be absolute path
is_executable :: proc(filename: string) -> bool {
  file_info, errno := os.stat(filename)
  if errno != 0 {
    return false
  }

  return file_info.mode & os.S_IXUSR != 0
}

// returns absolute path for a given program name.
// return nil is program is not executable
absolute_path :: proc(prog_name: string) -> ^string {
  // file in current directory?
  if strings.has_prefix(prog_name, "./") {
    fullname := fmt.tprintf("%s/%s", os.get_current_directory(), prog_name[1:])

    if is_executable(fullname) {
      return &fullname
    }
  }

  path_var := os.get_env("PATH")
  for dir in strings.split(path_var, ":") {
    fullname := fmt.tprintf("%s/%s", dir, prog_name)
    if is_executable(fullname) {
      return &fullname
    }
  }

  return nil
}

// reads line from stdin
read_input :: proc() -> Maybe(string) {
  data := make([dynamic]u8, 0)
  stream := os.stream_from_handle(os.stdin)
  reader, ok := io.to_reader(stream)
  if !ok {
    return nil
  }

  for {
    b, err := io.read_byte(reader)
    if err != nil {
      return nil
    }

    if b == 10 { // ENTER
      break
    }

    append(&data, b)
  }

  return string(data[:])
}

main :: proc() {
  for true {
    // print prompt. TODO: make this dynamic somehow? config file?
    fmt.printf("%s > ", os.get_current_directory())

    // read user input. command and arguments
    maybe_input := read_input()
    if maybe_input == nil {
      continue
    }
    input := maybe_input.?

    args := strings.split(input, " ")
    if len(args) == 0 {
      continue
    }

    // builtin command for changing current directory
    if args[0] == "cd" {
      path := args[1] if len(args) > 1 && args[1] != "" else "~"
      os.set_current_directory(path)
      continue
    }

    // convert arguments to []cstring for the syscall
    c_args := make([]cstring, len(args))
    for arg, i in args {
      if strings.trim_space(arg) == "" {
        continue
      }

      c_args[i] = strings.clone_to_cstring(arg, context.temp_allocator)
    }

    command_path := absolute_path(args[0])
    if command_path == nil {
      fmt.eprintf("%s: command not found\n", args[0])
      continue
    }
    // convert command into a cstring. it's a separate string just for execve call.
    // args[0] contains the name user used for invocation. standard linux convention.
    command := strings.clone_to_cstring(command_path^, context.temp_allocator)

    // fork in order to run execve in the child's process
    pid, errno := os.fork()
    if errno != os.ERROR_NONE {
      fmt.eprintln("Coulnd't fork. What?")
      os.exit(1)
    }

    if pid == 0 {
      // TODO: add env override support
      sys_execve(command, c_args, nil)
      os.exit(int(libc.errno()^))
    }

    // wait for child
    for wait_for_child() != 0 { }
    // clean up all the mess
    free_all(context.temp_allocator)
  }

  fmt.print("\n")
}